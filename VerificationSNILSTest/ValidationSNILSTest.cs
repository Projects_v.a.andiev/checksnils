
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Reflection;
using VerificationSNILS.Models;
using VerificationSNILS.Models.SNILS;



namespace VerificationSNILSTest
{
    [TestClass]
    public class ValidationSNILSTest
    {
        [TestMethod]
        public void SNILS_Count_11()
        {
            var checkFirstSnils = new ValidatorSNILS();

            var argsValues = new object[] { "07254235551" };
            var argsNames = new string[] { "snils" };

            var first = Convert.ToBoolean(TestPrivateStaticMethod<ValidatorSNILS>("CheckLengthSNLIS", argsValues, argsNames));

            argsValues = new object[] { @"0a49v49v@#$%^&*()_+\-/262700" };
            var second = Convert.ToBoolean(TestPrivateStaticMethod<ValidatorSNILS>("CheckLengthSNLIS", argsValues, argsNames));

            argsValues = new object[] { "199  04  348  910" };
            var three = Convert.ToBoolean(TestPrivateStaticMethod<ValidatorSNILS>("CheckLengthSNLIS", argsValues, argsNames));

            argsValues = new object[] { "07254235551" };
            var four = Convert.ToBoolean(TestPrivateStaticMethod<ValidatorSNILS>("CheckLengthSNLIS", argsValues, argsNames));

            argsValues = new object[] { "072542355512222222" };
            var five = Convert.ToBoolean(TestPrivateStaticMethod<ValidatorSNILS>("CheckLengthSNLIS", argsValues, argsNames));

            argsValues = new object[] { "07" };
            var six = Convert.ToBoolean(TestPrivateStaticMethod<ValidatorSNILS>("CheckLengthSNLIS", argsValues, argsNames));

            Assert.IsTrue(first);
            Assert.IsFalse(second);
            Assert.IsFalse(three);
            Assert.IsTrue(four);
            Assert.IsFalse(five);
            Assert.IsFalse(six);
        }

        [TestMethod]
        public void Is_StringNumber()
        {
            Assert.IsTrue(IsStringNumber(ValidatorSNILS.GetStringNumberSNILS(@"0a49v49v@#$%^&*()_+\-/262700,.|;:")));
        }


        [TestMethod]
        public void NumberSnils_Length_9()
        {
            Assert.AreEqual(ValidatorSNILS.GetNumberSNILS(@"07254235551").Length, 9);
        }

        [TestMethod]
        public void NumberSnils_Length_2()
        {
            Assert.AreEqual(ValidatorSNILS.GetContolNumber(@"07254235551").Length, 2);
        }

        [TestMethod]
        public void Is_ArrayInt_Count_9()
        {
            ValidatorSNILS.GetFormattedArrayIntSnils(@"123456789");

            Assert.AreEqual(ValidatorSNILS.GetFormattedArrayIntSnils(@"123456789").Length, 9);
            Assert.IsTrue(ValidatorSNILS.GetFormattedArrayIntSnils(@"123456789123").Length> 9);
            Assert.IsTrue(ValidatorSNILS.GetFormattedArrayIntSnils(@"3").Length < 9);
        }

        [TestMethod]
        public void VerifiableNumber_Check()
        {
            Assert.AreEqual(ValidatorSNILS.GetVerifiableNumber(new int[] { 0, 7, 2, 5, 4, 2, 3, 5, 5 }), 152);
            Assert.AreEqual(ValidatorSNILS.GetVerifiableNumber(new int[] { 0, 3, 3, 8, 2, 8, 7, 8, 9 }), 181);
            Assert.AreEqual(ValidatorSNILS.GetVerifiableNumber(new int[] { 6, 7, 1, 2, 9, 7, 8, 6, 0 }), 238);
        }

        [TestMethod]
        public void VerifyComparingNumber()
        {
            Assert.AreEqual(ValidatorSNILS.GetComparingNumber(152), 51);
            Assert.AreEqual(ValidatorSNILS.GetComparingNumber(181), 80);
            Assert.AreEqual(ValidatorSNILS.GetComparingNumber(238), 36);
        }


        [TestMethod]
        public void CheckControlNumber()
        {
            var argsValues = new object[] { 100, "00" };
            var argsNames = new string[] { "verifiableNumber", "controlNumber" };

            var first = Convert.ToBoolean(TestPrivateStaticMethod<ValidatorSNILS>("CheckControlNumber", argsValues, argsNames) );

            argsValues = new object[] { 25, "25" };
            var second = Convert.ToBoolean(TestPrivateStaticMethod<ValidatorSNILS>("CheckControlNumber", argsValues, argsNames) );

            argsValues = new object[] { 30, "30" };
            var three = Convert.ToBoolean(TestPrivateStaticMethod<ValidatorSNILS>("CheckControlNumber", argsValues, argsNames) );

            argsValues = new object[] { 30, "87" };
            var four = Convert.ToBoolean(TestPrivateStaticMethod<ValidatorSNILS>("CheckControlNumber", argsValues, argsNames));

            Assert.AreEqual(first, true);
            Assert.AreEqual(second, true);
            Assert.AreEqual(three, true);
            Assert.AreEqual(four, false);
        }


        [TestMethod]
        public void RepeatThreeSymols()
        {
            var argsValues = new object[] { "072542555" };
            var argsNames = new string[] { "snils" };

            var first = Convert.ToBoolean(TestPrivateStaticMethod<ValidatorSNILS>("IsRepeatThreeSymols", argsValues, argsNames));

            argsValues = new object[] { "000542355" };
            var second = Convert.ToBoolean(TestPrivateStaticMethod<ValidatorSNILS>("IsRepeatThreeSymols", argsValues, argsNames));

            argsValues = new object[] { "077742355" };
            var three = Convert.ToBoolean(TestPrivateStaticMethod<ValidatorSNILS>("IsRepeatThreeSymols", argsValues, argsNames));

            argsValues = new object[] { "072542355" };
            var four = Convert.ToBoolean(TestPrivateStaticMethod<ValidatorSNILS>("IsRepeatThreeSymols", argsValues, argsNames));

            Assert.AreEqual(first, true);
            Assert.AreEqual(second, true);
            Assert.AreEqual(three, true);
            Assert.AreEqual(four, false);
        }

        public object TestPrivateStaticMethod<T>(string nameMethod, object[] argsValues, string[] argsNames)
        {
            var method = typeof(T);
            return
            method.InvokeMember(nameMethod, BindingFlags.InvokeMethod | BindingFlags.NonPublic | BindingFlags.Static,
            null, null, argsValues, null, null, argsNames);
        }

        public object[] GetargsValues(object[] args)
        {
            return new object[] { args };
        }

        public bool IsStringNumber(string checkString)
        {
            foreach (var c in checkString)
            {
                if (!Char.IsNumber(c))
                {
                    return false;
                }
            }
            return true;
        }
    }
}
