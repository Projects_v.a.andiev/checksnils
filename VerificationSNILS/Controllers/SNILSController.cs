﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using VerificationSNILS.Models;
using VerificationSNILS.Models.SNILS;

namespace VerificationSNILS.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SNILSController : Controller
    {
        private IValidatorSNILS _validatorSNILS;

        public SNILSController(IValidatorSNILS validatorSNILS)
        {
            _validatorSNILS = validatorSNILS;
        }

        /// <summary>
        /// Проверка на валидность СНИЛС
        /// </summary>
        /// <remarks>
        /// Пример СНИЛС
        ///  072-542-355 51
        /// </remarks>
        /// <param name="snils"></param>
        /// <returns></returns>
        [HttpGet("Verification/{snils}")]
        public ActionResult<SNILSModel> VerificationSNILS(string snils)
        {
            try
            {
                return Ok(_validatorSNILS.CheckSnils(new SNILSModel(snils)));
            }
            catch (Exception e)
            {

               return this.BadRequest(e.GetBaseException().Message);
            }
            
        }
      
    }
}