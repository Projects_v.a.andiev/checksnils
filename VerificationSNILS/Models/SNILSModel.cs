﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using VerificationSNILS.Models.SNILS;

namespace VerificationSNILS.Models
{
    [Serializable]
    public class SNILSModel
    {
        
        public SNILSModel(string snils)
        {
            FullNumberSNILS = ValidatorSNILS.GetStringNumberSNILS(snils);
            NumberSNILS = ValidatorSNILS.GetNumberSNILS(FullNumberSNILS);
            ControlNumber = ValidatorSNILS.GetContolNumber(FullNumberSNILS);
            VerifiableNumber = ValidatorSNILS.GetVerifiableNumber(ValidatorSNILS.GetFormattedArrayIntSnils(NumberSNILS));
            ComparingNumber = ValidatorSNILS.GetComparingNumber(VerifiableNumber);
        }

        [Required]

        private string _fullNumberSNILS;
        /// <summary>
        /// Полный номер снилс с котрольным числом, обязательно 11 символов
        /// </summary>
        public string FullNumberSNILS
        {
            get { return _fullNumberSNILS; }
            set { _fullNumberSNILS = value; }
        }

        private string _numberSNILS;
        /// <summary>
        /// Номер СНИСЛ без последних двух чисел, так как это контрольное число, обязательно 9 символов
        /// </summary>
        public string NumberSNILS
        {
            get { return _numberSNILS; }
            set { _numberSNILS = value; }
        }


        private string _contolNumber;
        /// <summary>
        /// Два Последних числа, для сравнения контрольной суммы
        /// </summary>
        public string ControlNumber
        {
            get { return _contolNumber; }
            set { _contolNumber = value; }
        }

        private int _verifiableNumber;
        /// <summary>
        /// Котрольное число СНИЛС(перемноженные числа на их индекс в обратном порядке и сложенные между собой)
        /// </summary>
        public int VerifiableNumber
        {
            get { return _verifiableNumber; }
            set { _verifiableNumber = value; }
        }

        private int _comparingNumber;
        /// <summary>
        /// Если VerifiableNumber больше 101, тогда его делят пока оно не станет меньше либо равно 101 и на основе него идет проверка.
        /// </summary>
        public int ComparingNumber
        {
            get { return _comparingNumber; }
            set { _comparingNumber = value; }
        }


        private bool _isVereficationSNILS;
        /// <summary>
        /// Статус прошел проверку СНИЛС или нет.
        /// </summary>
        public bool IsVereficationSNILS
        {
            get { return _isVereficationSNILS; }
            set { _isVereficationSNILS = value; }
        }
    }
}
