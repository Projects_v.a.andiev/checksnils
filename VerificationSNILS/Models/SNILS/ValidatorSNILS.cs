﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace VerificationSNILS.Models.SNILS
{
    [Serializable]
    public class ValidatorSNILS : IValidatorSNILS
    {
        private static bool CheckLengthSNLIS(string snils)
        {
            return snils.Count() == 11;
        }

        public static string GetStringNumberSNILS(string snils)
        {
            return Regex.Replace(snils, "[^0-9]", "");
        }


        public static string GetNumberSNILS(string snils)
        {
            return new string(snils.Take(9).ToArray());
        }

        public static string GetContolNumber(string snils)
        {
            return new string(snils.Skip(9).Take(2).ToArray());
        }

        public static int GetVerifiableNumber(int[] snils)
        {
            var controlNumber = 0;
            for (int i = 1; i < snils.Count() + 1; i++)
            {
                controlNumber = controlNumber + snils[i - 1] * (10 - i);
            }
            return controlNumber;
        }


        public static int[] GetFormattedArrayIntSnils(string snils)
        {
            int[] formattedSNILS = new int[snils.Length];
            int j = 0;
            for (int i = 0; snils.Length > i && formattedSNILS.Length > j; i++)
            {
                if (Char.IsNumber(snils[i]))
                {
                    formattedSNILS[j] = Convert.ToInt32(new string(snils[i], 1));
                    j++;
                }
            }
            return formattedSNILS;
        }

        public SNILSModel CheckSnils(SNILSModel snils)
        {

            if (IsRepeatThreeSymols(snils.NumberSNILS))
            {               
                snils.IsVereficationSNILS = false;
                throw new Exception("Неправильный формат СНИСЛ!!!\nПовторяется три одинаковых числа подряд");
            }

            if (!CheckLengthSNLIS(snils.FullNumberSNILS))
            {
                throw new Exception($"Длина СНИСЛ дожна быть равна 11 символов!!!\nВвели СНИЛС длинной {snils.FullNumberSNILS.Count()}");
            }
            snils.IsVereficationSNILS = CheckControlNumber(snils.ComparingNumber, snils.ControlNumber);
            return snils;
        }

        public static int GetComparingNumber(int verifiableNumber)
        {
            if (verifiableNumber > 101)
            {
                verifiableNumber = GetComparingNumber(verifiableNumber % 101);
            }
            return verifiableNumber;
        }


        private static bool CheckControlNumber(int verifiableNumber, string controlNumber)
        {
            if ((verifiableNumber == 100 || verifiableNumber == 101) && controlNumber.ToString() == "00")
            {
                return true;
            }

            if (verifiableNumber < 100)
            {
                if (verifiableNumber == Convert.ToInt32(controlNumber))
                {
                    return true;
                }
                return false;
            }
            return false;
        }

        private static bool IsRepeatThreeSymols(string snils)
        {
            for (int i = 0; i < snils.Length; i++)
            {
                if (snils.Contains($"{i}{i}{i}"))
                {
                    return true;
                }
            }
            return false;
        }
    }
}

