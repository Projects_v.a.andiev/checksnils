﻿using System;

namespace VerificationSNILS.Models.SNILS
{
   
    public interface IValidatorSNILS
    {
        SNILSModel CheckSnils(SNILSModel snils);
        //void Inizialize(string snilsNumber);
    }
}