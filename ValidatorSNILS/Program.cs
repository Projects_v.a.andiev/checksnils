﻿using System;
using System.Linq;

namespace ValidatorSNILS
{
    class Program
    {
        static void Main(string[] args)
        {
            var snilsChecker = new SNILS("072-542-355 51");
            var snilsChecker2 = new SNILS("04949262700");
            var snilsChecker3 = new SNILS("19904348910");
            var snilsChecker4 = new SNILS("07254235551");
            snilsChecker.CheckSnils();

            Console.WriteLine(snilsChecker.CheckSnils());
            Console.WriteLine(snilsChecker2.CheckSnils());
            Console.WriteLine(snilsChecker3.CheckSnils());
            Console.WriteLine(snilsChecker4.CheckSnils());
        }

       
    }
}
