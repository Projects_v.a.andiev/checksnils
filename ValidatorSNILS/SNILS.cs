﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ValidatorSNILS
{
    public class SNILS
    {

        private string _numberSNILS;

        public string NumberSNILS
        {
            get { return _numberSNILS; }
            set { _numberSNILS = value; }
        }


        private string _contolNumber;

        public string ControlNumber
        {
            get { return _contolNumber; }
            set { _contolNumber = value; }
        }

        private int _verifiableNumber;

        public int VerifiableNumber
        {
            get { return _verifiableNumber; }
            set { _verifiableNumber = value; }
        }

        private int _comparingNumber;

        public int ComparingNumber
        {
            get { return _comparingNumber; }
            set { _comparingNumber = value; }
        }

        public SNILS(string snilsNumber)
        {
            _numberSNILS = GetNumberSNILS (snilsNumber);
            _contolNumber = GetContolNumber(snilsNumber);
            _verifiableNumber = GetVerifiableNumber(GetFormattedArrayIntSnils(_numberSNILS,9));
            _comparingNumber = GetComparingNumber(_verifiableNumber);
        }

        private static string GetNumberSNILS(string snils)
        {
            try
            {
                var formattedSnils = String.Concat(GetFormattedArrayIntSnils(snils,11));
                return new string(formattedSnils.Take(9).ToArray());
            }
            catch (Exception e)
            {

                throw e.InnerException;
            }
        }

        private static string GetContolNumber(string snils)
        {
            try
            {
                var formattedSnils = String.Concat(GetFormattedArrayIntSnils(snils,11));
                return new string(formattedSnils.Skip(9).Take(2).ToArray());
            }
            catch (Exception e)
            {

                throw e.InnerException;
            }

        }

        private static int GetVerifiableNumber(int[] snils)
        {
            try
            {
                var controlNumber = 0;
                for (int i = 1; i < snils.Count() + 1; i++)
                {
                    controlNumber = controlNumber + snils[i - 1] * (10 - i);
                }
                return controlNumber;
            }
            catch (Exception e)
            {

                throw e.InnerException;
            }
        }


        private static int[] GetFormattedArrayIntSnils(string snils,int lengthArr)
        {
            try
            {
                int[] formattedSNILS = new int[lengthArr];
                int j = 0;
                for (int i = 0; snils.Length > i && formattedSNILS.Length > j; i++)
                {
                    if (Char.IsNumber(snils[i]))
                    {
                        formattedSNILS[j] = Convert.ToInt32(new string(snils[i], 1));
                        j++;
                    }
                }
                return formattedSNILS;
            }
            catch (Exception e)
            {

                throw e.InnerException;
            }
        }

        public bool CheckSnils()
        {
            try
            {
                if (IsRepeatThreeSymols(_numberSNILS))
                {
                    return false;
                }

                return CheckControlNumber(_comparingNumber, _contolNumber);
            }
            catch (Exception e)
            {

                throw e.InnerException;
            }
        }

        private int GetComparingNumber(int verifiableNumber)
        {
            try
            {
                if (verifiableNumber > 101)
                {
                    verifiableNumber = GetComparingNumber(verifiableNumber % 101);
                }
                return verifiableNumber;
            }
            catch (Exception e)
            {

                throw e.InnerException;
            }
        }


        private static bool CheckControlNumber(int verifiableNumber, string controlNumber)
        {
            try
            {
                if ((verifiableNumber == 100 || verifiableNumber == 101) && controlNumber.ToString() == "00")
                {
                    return true;
                }

                if (controlNumber == "00")
                {
                    return CheckControlNumber(verifiableNumber, "00");
                }

                if (verifiableNumber < 100)
                {
                    if (verifiableNumber == Convert.ToInt32(controlNumber))
                    {
                        return true;
                    }
                    return false;
                }
                return false;
            }
            catch (Exception e)
            {

                throw e.InnerException;
            }
        }

        private static bool IsRepeatThreeSymols(string snils)
        {
            try
            {
                for (int i = 0; i < snils.Length; i++)
                {
                    if (snils.Contains($"{i}{i}{i}"))
                    {
                        return true;
                    }
                }
                return false;
            }
            catch (Exception e)
            {
                throw e.InnerException;
            }
        }
    }
}
